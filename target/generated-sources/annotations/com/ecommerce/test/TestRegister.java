
package com.ecommerce.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestRegister {

	public static void main(String[] args) {

        // Configurar el path del WebDriver
        System.setProperty("webdriver.chrome.driver", "F:\\Workspace\\EcommerceTest\\resources\\chromedriver2.exe");

        // Opciones de Chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-gpu");
        options.addArguments("--headless"); // Opcional: ejecuta Chrome en modo sin cabeza
        options.addArguments("--disable-software-rasterizer");

        // Inicializar el ChromeDriver
        WebDriver driver = new ChromeDriver(options);

        try {
            // Navegar a la p�gina de registro de la web e-commerce
            driver.get("https://www.naturalrasta.com.ar/register.html");

            // Esperar a que la p�gina cargue completamente
            Thread.sleep(2000);

            // Caso 1: No completar los campos obligatorios
            WebElement registerButton = driver.findElement(By.id("registerButton"));
            registerButton.click();
            Thread.sleep(2000);
            WebElement errorMessage = driver.findElement(By.id("errorMessage"));
            if (errorMessage.isDisplayed() && errorMessage.getText().equals("Todos los campos son obligatorios.")) {
                System.out.println("Validaci�n de campos obligatorios pas�.");
            } else {
                System.out.println("Validaci�n de campos obligatorios fall�.");
            }

            // Caso 2: Ingresar contrase�a m�s corta de lo requerido
            WebElement usernameField = driver.findElement(By.id("username"));
            WebElement emailField = driver.findElement(By.id("email"));
            WebElement passwordField = driver.findElement(By.id("password"));
            WebElement confirmPasswordField = driver.findElement(By.id("confirmPassword"));
            WebElement dobField = driver.findElement(By.id("dob"));

            usernameField.sendKeys("nuevoUsuario");
            emailField.sendKeys("usuario@example.com");
            passwordField.sendKeys("short");
            confirmPasswordField.sendKeys("short");
            dobField.sendKeys("2000-01-01");
            registerButton.click();
            Thread.sleep(2000);
            if (errorMessage.isDisplayed() && errorMessage.getText().equals("La contrase�a debe tener al menos 8 caracteres.")) {
                System.out.println("Validaci�n de contrase�a corta pas�.");
            } else {
                System.out.println("Validaci�n de contrase�a corta fall�.");
            }

            // Caso 3: Poner una fecha de nacimiento mayor a la fecha actual
            passwordField.clear();
            confirmPasswordField.clear();
            dobField.clear();

            passwordField.sendKeys("contrase�aSegura");
            confirmPasswordField.sendKeys("contrase�aSegura");
            dobField.sendKeys("2050-01-01");
            registerButton.click();
            Thread.sleep(2000);
            if (errorMessage.isDisplayed() && errorMessage.getText().equals("La fecha de nacimiento no puede ser en el futuro.")) {
                System.out.println("Validaci�n de fecha de nacimiento pas�.");
            } else {
                System.out.println("Validaci�n de fecha de nacimiento fall�.");
            }

            // Caso 4: Registro exitoso
            dobField.clear();
            dobField.sendKeys("2000-01-01");
            registerButton.click();
            Thread.sleep(2000);
            WebElement successMessage = driver.findElement(By.id("successMessage"));
            if (successMessage.isDisplayed()) {
                System.out.println("Registro completado con �xito.");
            } else {
                System.out.println("Fallo en el registro.");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Cerrar el navegador
            driver.quit();
        }
	}

}
