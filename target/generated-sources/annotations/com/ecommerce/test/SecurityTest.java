package com.ecommerce.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SecurityTest {

    public static void main(String[] args) {
        // Configurar el path del WebDriver
        System.setProperty("webdriver.chrome.driver", "F:\\Workspace Servlet\\EcommerceTest2024\\resources\\chromedriver2.exe");

        // Opciones de Chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-gpu");
        options.addArguments("--headless"); // Opcional: ejecuta Chrome en modo sin cabeza
        options.addArguments("--disable-software-rasterizer");

        // Inicializar el ChromeDriver
        WebDriver driver = new ChromeDriver(options);

        try {
            // Navegar a la p�gina de registro
            driver.get("https://www.naturalrasta.com.ar/register.html");

            // Esperar a que la p�gina cargue completamente
            Thread.sleep(2000);

            // Localizar los elementos del formulario de registro
            WebElement usernameField = driver.findElement(By.id("username"));  // Actualiza los IDs si son diferentes
            WebElement emailField = driver.findElement(By.id("email"));
            WebElement passwordField = driver.findElement(By.id("password"));
            WebElement confirmPasswordField = driver.findElement(By.id("confirmPassword"));
            WebElement registerButton = driver.findElement(By.id("registerButton"));

            // Prueba de inyecci�n de SQL en el campo de nombre de usuario
            usernameField.sendKeys("' OR '1'='1");
            emailField.sendKeys("test@example.com");
            passwordField.sendKeys("password");
            confirmPasswordField.sendKeys("password");

            // Hacer clic en el bot�n de registro
            registerButton.click();

            // Esperar a que se procese el registro
            Thread.sleep(3000);

            // Verificar la respuesta
            String pageSource = driver.getPageSource();
            if (pageSource.contains("Error") || pageSource.contains("error")) {
                System.out.println("Vulnerabilidad detectada: posible inyecci�n de SQL.");
            } else {
                System.out.println("No se detectaron vulnerabilidades de inyecci�n de SQL.");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Cerrar el navegador
            driver.quit();
        }
    }
}
