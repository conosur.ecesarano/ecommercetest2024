package com.ecommerce.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AddToCartTest {

    public static void main(String[] args) {
        // Configurar el path del WebDriver
        System.setProperty("webdriver.chrome.driver", "F:\\Workspace Servlet\\EcommerceTest2024\\resources\\chromedriver2.exe");

        // Opciones de Chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-gpu");
        options.addArguments("--headless"); // Opcional: ejecuta Chrome en modo sin cabeza
        options.addArguments("--disable-software-rasterizer");

        // Inicializar el ChromeDriver
        WebDriver driver = new ChromeDriver(options);

        try {
            // Navegar a la pagina de la tienda en linea
            driver.get("https://www.naturalrasta.com.ar/store.html");

            // Esperar a que la pagina cargue completamente
            Thread.sleep(2000);

            // Caso 1: Agregar Producto 1 al carrito
            WebElement addProduct1Button = driver.findElement(By.id("addProduct1Button"));
            addProduct1Button.click();
            Thread.sleep(2000);
            WebElement cartList = driver.findElement(By.id("cartList"));
            if (cartList.getText().contains("Producto 1")) {
                System.out.println("Producto 1 agregado al carrito con exito.");
            } else {
                System.out.println("Fallo al agregar Producto 1 al carrito.");
            }

            // Caso 2: Agregar Producto 2 al carrito
            WebElement addProduct2Button = driver.findElement(By.id("addProduct2Button"));
            addProduct2Button.click();
            Thread.sleep(2000);
            if (cartList.getText().contains("Producto 2")) {
                System.out.println("Producto 2 agregado al carrito con exito.");
            } else {
                System.out.println("Fallo al agregar Producto 2 al carrito.");
            }

            // Caso 3: Quitar Producto 1 del carrito
            WebElement removeButton1 = cartList.findElement(By.cssSelector("li[data-product-id='1'] .remove-button"));
            removeButton1.click();
            Thread.sleep(2000);
            if (!cartList.getText().contains("Producto 1")) {
                System.out.println("Producto 1 eliminado del carrito con exito.");
            } else {
                System.out.println("Fallo al eliminar Producto 1 del carrito.");
            }

            // Caso 4: Quitar Producto 2 del carrito
            WebElement removeButton2 = cartList.findElement(By.cssSelector("li[data-product-id='2'] .remove-button"));
            removeButton2.click();
            Thread.sleep(2000);
            if (!cartList.getText().contains("Producto 2")) {
                System.out.println("Producto 2 eliminado del carrito con exito.");
            } else {
                System.out.println("Fallo al eliminar Producto 2 del carrito.");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Cerrar el navegador
            driver.quit();
        }
    }
}
