package com.ecommerce.test;

import org.openqa.selenium.By;  
import org.openqa.selenium.JavascriptExecutor;  
import org.openqa.selenium.WebDriver;  
import org.openqa.selenium.chrome.ChromeDriver;  

public class UserRegistrationTest {
	
	
    public static void main(String[] args) {
        // Configurar el path del WebDriver
        System.setProperty("webdriver.chrome.driver", "F:\\Workspace Servlet\\EcommerceTest2024\\resources\\chromedriver.exe");

        // Crear una instancia del WebDriver
        WebDriver driver = new ChromeDriver();

        try {
            // Navegar a la p�gina de registro de la web e-commerce
            driver.get("https://www.tu-ecommerce.com/register");

            // Esperar a que la p�gina cargue completamente
            Thread.sleep(2000);

            // Localizar los elementos del formulario de registro
            WebElement usernameField = driver.findElement(By.id("username"));
            WebElement emailField = driver.findElement(By.id("email"));
            WebElement passwordField = driver.findElement(By.id("password"));
            WebElement confirmPasswordField = driver.findElement(By.id("confirmPassword"));
            WebElement registerButton = driver.findElement(By.id("registerButton"));

            // Completar el formulario de registro
            usernameField.sendKeys("nuevoUsuario");
            emailField.sendKeys("usuario@example.com");
            passwordField.sendKeys("contrase�aSegura");
            confirmPasswordField.sendKeys("contrase�aSegura");

            // Hacer clic en el bot�n de registro
            registerButton.click();

            // Esperar a que se procese el registro
            Thread.sleep(3000);

            // Verificar que el registro fue exitoso
            WebElement successMessage = driver.findElement(By.id("successMessage"));
            if (successMessage.isDisplayed()) {
                System.out.println("Registro completado con �xito.");
            } else {
                System.out.println("Fallo en el registro.");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Cerrar el navegador
            driver.quit();
        }
    }
}
